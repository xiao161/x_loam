# x_loam
Just a Lidar(not livox) Odometry(No IMU.GNSS.or other constraints and special strategies).


## What  basic conditions are needed
1. Need Ubuntu system,ROS system
2. Need pcl,Eigen,Ceres
3. Need Ros dataset bag(have "velodyne"toic)

## How to build this project
 cd ~/X_loam
   
 catkin_make -j4 

## How to run
roslaunch ***/aloam_velodyne_VLP_16.launch

rosbag play *.bag

