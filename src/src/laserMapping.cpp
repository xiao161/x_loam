// This is an advanced implementation of the algorithm described in the following paper:
//   J. Zhang and S. Singh. LOAM: Lidar Odometry and Mapping in Real-time.
//     Robotics: Science and Systems Conference (RSS). Berkeley, CA, July 2014. 

// Modifier: Tong Qin               qintonguav@gmail.com
// 	         Shaozu Cao 		    saozu.cao@connect.ust.hk


// Copyright 2013, Ji Zhang, Carnegie Mellon University
// Further contributions copyright (c) 2016, Southwest Research Institute
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <math.h>
#include <vector>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>

#include <pcl/search/impl/search.hpp>
#include <pcl/range_image/range_image.h>

#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>

#include <pcl/filters/crop_box.h> 


#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>


#include <eigen3/Eigen/Dense>
#include <ceres/ceres.h>
#include <mutex>
#include <queue>
#include <thread>
#include <iostream>
#include <string>
#include <list>
#include <set>

#include "lidarFactor.hpp"
#include "tic_toc.h"
#include "optmization.h"
#include "common.h"

int frameCount = 0;

double timeLaserCloudCornerLast = 0;
double timeLaserCloudSurfLast = 0;
double timeLaserCloudFullRes = 0;
double timeLaserOdometry = 0;

//livox
double m_inlier_threshold;
int    m_maximum_allow_residual_block = 1e5;
double m_inlier_ratio = 0.80;
double m_inliner_dis = 0.02;
double m_residuals_len_threshold=0.1;

int laserCloudValidInd[125];
int laserCloudSurroundInd[125];

// input: from odom
pcl::PointCloud<PointType>::Ptr laserCloudCornerLast(new pcl::PointCloud<PointType>());
pcl::PointCloud<PointType>::Ptr laserCloudSurfLast(new pcl::PointCloud<PointType>());

// ouput: all visualble cube points
pcl::PointCloud<PointType>::Ptr laserCloudSurround(new pcl::PointCloud<PointType>());

// surround points in map to build tree
pcl::PointCloud<PointType>::Ptr laserCloudCornerFromMap(new pcl::PointCloud<PointType>());
pcl::PointCloud<PointType>::Ptr laserCloudSurfFromMap(new pcl::PointCloud<PointType>());

//input & output: points in one frame. local --> global
pcl::PointCloud<PointType>::Ptr laserCloudFullRes(new pcl::PointCloud<PointType>());

//kd-tree
pcl::KdTreeFLANN<PointType>::Ptr kdtreeCornerFromMap(new pcl::KdTreeFLANN<PointType>());
pcl::KdTreeFLANN<PointType>::Ptr kdtreeSurfFromMap(new pcl::KdTreeFLANN<PointType>());

double parameters[7] = {0, 0, 0, 1, 0, 0, 0};
Eigen::Map<Eigen::Quaterniond> q_w_curr(parameters);
Eigen::Map<Eigen::Vector3d> t_w_curr(parameters + 4);

Eigen::Quaterniond q_wmap_wodom(1, 0, 0, 0);
Eigen::Vector3d t_wmap_wodom(0, 0, 0);

Eigen::Quaterniond q_wodom_curr(1, 0, 0, 0);
Eigen::Vector3d t_wodom_curr(0, 0, 0);


std::queue<sensor_msgs::PointCloud2ConstPtr> cornerLastBuf;
std::queue<sensor_msgs::PointCloud2ConstPtr> surfLastBuf;
std::queue<sensor_msgs::PointCloud2ConstPtr> fullResBuf;
std::queue<nav_msgs::Odometry::ConstPtr> odometryBuf;
std::vector<geometry_msgs::PoseStamped> posestapedBuf;
std::vector<geometry_msgs::PoseStamped> posestapeBufTemp;
std::mutex mBuf;
std::mutex mOptmization;
std::mutex mGpsBuf;
// change the localmap 
struct Pose_Time{
	double posetime;
	Eigen::Vector3d t;
	Eigen::Quaterniond q;
};
//gps constraint
std::map<double,pcl::PointCloud<PointType>> CornerCloudMsgBuf;
std::map<double,pcl::PointCloud<PointType>> SurfCloudMsgBuf;
//std::map<double,pcl::PointCloud<PointType>> FullCloudMsgBuf;
std::vector<Pose_Time> ConditionCornerLidarPoseTimeBuf ;
float LocalMapRangeXY = 100.0;    //m room 60.0
float LocalMapRangeZ = 10.30;    //m  room      1.30
float LocalMapTimeRange = 20.0;// s   time    50.0s  
int m_current_frame_index = 0;//cloud index
int m_loop_closure_maximum_keyframe_in_wating_list = 0;

//m_para_scans_between_two_keyframe < m_para_scans_of_each_keyframe
int m_para_scans_of_each_keyframe = 15;
int m_maximum_history_size = 400;
int m_para_scans_between_two_keyframe = 10; 
float m_loop_closure_minimum_similarity_linear = 0.65;
float m_loop_closure_minimum_similarity_planar = 0.95;
std::list<pcl::PointCloud<PointType>> m_laser_cloud_full_history;
int  m_loop_closure_minimum_keyframe_differen = 40;
std::mutex m_mutex_keyframe;

pcl::VoxelGrid<PointType> downSizeFilterCorner;
pcl::VoxelGrid<PointType> downSizeFilterSurf;

std::vector<int> pointSearchInd;
std::vector<float> pointSearchSqDis;

PointType pointOri, pointSel;

ros::Publisher pubLaserCloudSurround, pubLaserCloudMap, pubLaserCloudFullRes, pubOdomAftMapped, pubOdomAftMappedHighFrec, pubLaserAfterMappedPath;
ros::Publisher pubLaserOdometryIncremental;
nav_msgs::Path laserAfterMappedPath;

bool GpsPositionOptmizationFlag = false ;
bool LoopOptmizationFlag = false;
bool newGps = false;

/*
    * A point cloud type that has 6D pose info ([x,y,z,roll,pitch,yaw] intensity is time stamp)
    */
struct PointXYZIRPYT
{
    PCL_ADD_POINT4D
    PCL_ADD_INTENSITY;                  // preferred way of adding a XYZ+padding
    float roll;
    float pitch;
    float yaw;
    double time;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIRPYT,
                                   (float, x, x) (float, y, y)
                                   (float, z, z) (float, intensity, intensity)
                                   (float, roll, roll) (float, pitch, pitch) (float, yaw, yaw)
                                   (double, time, time))

typedef PointXYZIRPYT  PointTypePose;


Eigen::Affine3f incrementalOdometryAffineFront;
Eigen::Affine3f incrementalOdometryAffineBack;
pcl::PointCloud<PointType>::Ptr cloudKeyPoses3D;
pcl::PointCloud<PointTypePose>::Ptr cloudKeyPoses6D;
pcl::PointCloud<PointType>::Ptr copy_cloudKeyPoses3D;


// set initial guess
void transformAssociateToMap()
{
	q_w_curr = q_wmap_wodom * q_wodom_curr;
	t_w_curr = q_wmap_wodom * t_wodom_curr + t_wmap_wodom;
}

void transformUpdate()
{
	q_wmap_wodom = q_w_curr * q_wodom_curr.inverse();
	t_wmap_wodom = t_w_curr - q_wmap_wodom * t_wodom_curr;

#if 1
	ROS_INFO_STREAM("LaserOdo==========================================");
	
	ROS_INFO_STREAM("LaserOdo t_w_curr x:" << t_w_curr[0] <<" y:" << t_w_curr[1] <<" z:"<< t_w_curr[2]);
	ROS_INFO_STREAM("LaserOdo t_wodom_curr x:" << t_wodom_curr[0] <<" y:" << t_wodom_curr[1] <<" z:"<< t_wodom_curr[2]);
	ROS_INFO_STREAM("LaserOdot_wap_wodom: x:" << t_wmap_wodom[0] << " y:" <<t_wmap_wodom[1] <<" z:" <<
	        t_wmap_wodom[2]);

	Eigen::Vector3d att_w_curr = q2att(q_w_curr) *180.0 / M_PI;
	ROS_INFO_STREAM("LaserOdo q_w_curr pitch:" << att_w_curr[0] <<" roll:" << att_w_curr[1] << " yaw:"<< 
	               att_w_curr[2]);

	Eigen::Vector3d att_wodom_curr = q2att(q_wodom_curr) *180.0 / M_PI;
	ROS_INFO_STREAM("LaserOdo att_wodom_curr pitch:" << att_wodom_curr[0] <<" roll:" << att_wodom_curr[1] << " yaw:"<< 
	               att_wodom_curr[2]); 

	Eigen::Vector3d att_wmap_wodom = q2att(q_wmap_wodom) *180.0 / M_PI;
	ROS_INFO_STREAM("LaserOdo q_wap_wodom pitch:" << att_wmap_wodom[0] <<" roll:" << att_wmap_wodom[1] <<
	            " yaw:" << att_wmap_wodom[2]);
	ROS_INFO_STREAM("LaserOdo -------------------------------------------------");
#endif

}

void pointAssociateToMap(PointType const *const pi, PointType *const po)
{
	Eigen::Vector3d point_curr(pi->x, pi->y, pi->z);
	Eigen::Vector3d point_w = q_w_curr * point_curr + t_w_curr;
	po->x = point_w.x();
	po->y = point_w.y();
	po->z = point_w.z();
	po->intensity = pi->intensity;
	//po->intensity = 1.0;
}

void pointAssociateTobeMapped(PointType const *const pi, PointType *const po)
{
	Eigen::Vector3d point_w(pi->x, pi->y, pi->z);
	Eigen::Vector3d point_curr = q_w_curr.inverse() * (point_w - t_w_curr);
	po->x = point_curr.x();
	po->y = point_curr.y();
	po->z = point_curr.z();
	po->intensity = pi->intensity;
}
PointType lidarcloudTobeLocalMap(Pose_Time pose, const PointType pi)
{
	PointType po;
	Eigen::Vector3d point_curr(pi.x, pi.y, pi.z);
	Eigen::Vector3d point_w = pose.q * point_curr + pose.t;
	po.x = point_w.x();
	po.y = point_w.y();
	po.z = point_w.z();
	po.intensity = pi.intensity;	
	return po;
}
//update corner local map
void LocalMapPoseFix(double CurrTime, Eigen::Vector3d t_curr)
{
    ConditionCornerLidarPoseTimeBuf.clear();
	std::vector<Pose_Time> buffTemp ;
    mOptmization.lock();
	for(size_t i = 0; i < posestapedBuf.size(); i++)
	{
		geometry_msgs::PoseStamped LidarPose = posestapedBuf[i];
		Pose_Time pose_time;
		pose_time.posetime = LidarPose.header.stamp.toSec();
		pose_time.t[0] = LidarPose.pose.position.x;
		pose_time.t[1] = LidarPose.pose.position.y;
		pose_time.t[2] = LidarPose.pose.position.z;
		pose_time.q.w() = LidarPose.pose.orientation.w;
		pose_time.q.x() = LidarPose.pose.orientation.x;		
		pose_time.q.y() = LidarPose.pose.orientation.y;
		pose_time.q.z() = LidarPose.pose.orientation.z;
        buffTemp.push_back(pose_time);
		if(fabs(t_curr[0] - pose_time.t[0]) < LocalMapRangeXY && fabs(t_curr[1] - pose_time.t[1]) < LocalMapRangeXY &&
		   fabs(t_curr[2]- pose_time.t[2]) <  LocalMapRangeZ && fabs(CurrTime - pose_time.posetime) < LocalMapTimeRange )
		{
			ConditionCornerLidarPoseTimeBuf.push_back(pose_time);
		}				 
	}
	if(ConditionCornerLidarPoseTimeBuf.size() < 5 )
	{
		ConditionCornerLidarPoseTimeBuf.clear();
		size_t size = buffTemp.size();
		for(size_t i = 0; i < size; i++)
		{
          ConditionCornerLidarPoseTimeBuf.push_back( buffTemp[i] );
		}		 		
	}
	mOptmization.unlock();
}

void UpdateLocalMap(std::map<double,pcl::PointCloud<PointType>> &CloudMsgBuf, 
					pcl::PointCloud<PointType>::Ptr localMap,
					pcl::VoxelGrid<PointType> VoxelFilter)
{
	pcl::PointCloud<PointType>::Ptr mapTemp(new pcl::PointCloud<PointType>());
	for(size_t i = 0; i < ConditionCornerLidarPoseTimeBuf.size(); i++)
	{
		Pose_Time pose_time = ConditionCornerLidarPoseTimeBuf[i];
		std::map<double,pcl::PointCloud<PointType>>::iterator it = CloudMsgBuf.find(pose_time.posetime);
		if( it == CloudMsgBuf.end())
		{
			std::cout<<"------------ CloudMsgBuf  error!------"<<std::endl;
		}
        else
		{
			mapTemp->clear();
			auto CloudTemp = it->second;
            VoxelFilter.setInputCloud(CloudTemp.makeShared());
			VoxelFilter.filter(*mapTemp);
            for(size_t j = 0; j < mapTemp->size();j++)
			{
				localMap->push_back(lidarcloudTobeLocalMap(pose_time,mapTemp->points[j]));
			} 
		}
	}
}

void laserCloudCornerLastHandler(const sensor_msgs::PointCloud2ConstPtr &laserCloudCornerLast2)
{
	ROS_DEBUG("----------laserCloudCornerLastHandler\n");
	mBuf.lock();
	cornerLastBuf.push(laserCloudCornerLast2);
	mBuf.unlock();
}

void laserCloudSurfLastHandler(const sensor_msgs::PointCloud2ConstPtr &laserCloudSurfLast2)
{
	ROS_DEBUG("*----------laserCloudSurfLastHandler\n");
	mBuf.lock();
	surfLastBuf.push(laserCloudSurfLast2);
	mBuf.unlock();
}

void laserCloudFullResHandler(const sensor_msgs::PointCloud2ConstPtr &laserCloudFullRes2)
{
	ROS_DEBUG("*----------laserCloudFullResHandler\n");
	mBuf.lock();
	fullResBuf.push(laserCloudFullRes2);
	mBuf.unlock();
}

Eigen::Affine3f  getLaserBeforeOdometry(const nav_msgs::Odometry::ConstPtr &beforelaserOdometry)
{
	double time = beforelaserOdometry->header.stamp.toSec();
	ROS_DEBUG("laser odometry:%f\n",time);
	Eigen::Affine3f res;
	double roll, pitch, yaw;
    geometry_msgs::Quaternion geoQuat = beforelaserOdometry->pose.pose.orientation;
    tf::Matrix3x3(tf::Quaternion(geoQuat.x, geoQuat.y, geoQuat.z, geoQuat.w)).getRPY(roll, pitch, yaw);
	
	res  = pcl::getTransformation(beforelaserOdometry->pose.pose.position.x ,beforelaserOdometry->pose.pose.position.y,
											beforelaserOdometry->pose.pose.position.z,roll , pitch, yaw);
    incrementalOdometryAffineFront = res;
    return res;
}


//receive odomtry
void laserOdometryHandler(const nav_msgs::Odometry::ConstPtr &laserOdometry)
{
	mBuf.lock();
	odometryBuf.push(laserOdometry);
	mBuf.unlock();

	// high frequence publish
	Eigen::Quaterniond q_wodom_curr;
	Eigen::Vector3d t_wodom_curr;
	q_wodom_curr.x() = laserOdometry->pose.pose.orientation.x;
	q_wodom_curr.y() = laserOdometry->pose.pose.orientation.y;
	q_wodom_curr.z() = laserOdometry->pose.pose.orientation.z;
	q_wodom_curr.w() = laserOdometry->pose.pose.orientation.w;
	t_wodom_curr.x() = laserOdometry->pose.pose.position.x;
	t_wodom_curr.y() = laserOdometry->pose.pose.position.y;
	t_wodom_curr.z() = laserOdometry->pose.pose.position.z;

	Eigen::Quaterniond q_w_curr = q_wmap_wodom * q_wodom_curr;
	Eigen::Vector3d t_w_curr = q_wmap_wodom * t_wodom_curr + t_wmap_wodom; 
	// q_w_curr = q_wmap_wodom * q_wodom_curr;
	// t_w_curr = q_wmap_wodom * t_wodom_curr + t_wmap_wodom; 

	nav_msgs::Odometry odomAftMapped;
	odomAftMapped.header.frame_id = "camera_init";
	odomAftMapped.child_frame_id = "aft_mapped";
	odomAftMapped.header.stamp = laserOdometry->header.stamp;
	odomAftMapped.pose.pose.orientation.x = q_w_curr.x();
	odomAftMapped.pose.pose.orientation.y = q_w_curr.y();
	odomAftMapped.pose.pose.orientation.z = q_w_curr.z();
	odomAftMapped.pose.pose.orientation.w = q_w_curr.w();
	odomAftMapped.pose.pose.position.x = t_w_curr.x();
	odomAftMapped.pose.pose.position.y = t_w_curr.y();
	odomAftMapped.pose.pose.position.z = t_w_curr.z();
	pubOdomAftMappedHighFrec.publish(odomAftMapped);
}
//livox
 double compute_inlier_residual_threshold( std::vector< double > residuals, double ratio )
  {
      std::set< double > dis_vec;
      for ( size_t i = 0; i < ( size_t )( residuals.size() / 3 ); i++ )
      {
          dis_vec.insert( fabs( residuals[ 3 * i + 0 ] ) + fabs( residuals[ 3 * i + 1 ] ) + fabs( residuals[ 3 * i + 2 ] ) );
      }
      return *( std::next( dis_vec.begin(), ( int ) ( ( ratio ) * dis_vec.size() ) ) );
  }

Eigen::Affine3f  eigen2Eigen(Eigen::Vector3d t, Eigen::Quaterniond q)
{
	Eigen::Vector3d att = q2att(q);// pitch roll yaw

	Eigen::Affine3f res;
	
	res  = pcl::getTransformation(t[0] ,t[1], t[2] ,att[1], att[0], att[2]);

    return res;
}

  void publishIncrementalOdometry(float pose[],double laserTime)
    {

        // Publish odometry for ROS (incremental)
        static bool lastIncreOdomPubFlag = false;
        static nav_msgs::Odometry laserOdomIncremental; // incremental odometry msg
        static Eigen::Affine3f increOdomAffine; // incremental odometry in affine
        if (lastIncreOdomPubFlag == false)
        {
            lastIncreOdomPubFlag = true;
            increOdomAffine = eigen2Eigen(t_w_curr,q_w_curr);
        } 
		else 
		{
            Eigen::Affine3f affineIncre = incrementalOdometryAffineFront.inverse() * incrementalOdometryAffineBack;
            increOdomAffine = increOdomAffine * affineIncre;

        	ROS_DEBUG("---------------------------------------------------\n");
        	float x2, y2, z2, roll2, pitch2, yaw2, len2;
        	pcl::getTranslationAndEulerAngles (affineIncre, x2, y2, z2, roll2, pitch2, yaw2);	
			len2 = sqrt(x2*x2 + y2*y2 + z2*z2);

    		ROS_INFO("lidar affin :%f  %f  %f  len:%f  att :%f   %f  %f\n",x2 , y2, z2, len2, roll2 ,  pitch2,  yaw2);		
        }

        float x, y, z, roll, pitch, yaw,len;
        pcl::getTranslationAndEulerAngles (increOdomAffine, x, y, z, roll, pitch, yaw);
        len = sqrt(x*x + y*y + z*z);        

        float x0, y0, z0, roll0, pitch0, yaw0,len0;
        pcl::getTranslationAndEulerAngles (incrementalOdometryAffineFront, x0, y0, z0, roll0, pitch0, yaw0);
        len0 = sqrt( x0*x0 + y0*y0 + z0*z0 );

        float x1, y1, z1, roll1, pitch1, yaw1,len1;
        pcl::getTranslationAndEulerAngles (incrementalOdometryAffineBack, x1, y1, z1, roll1, pitch1, yaw1);
        len1 = sqrt(x1*x1 + y1*y1 + z1*z1);



	    ROS_INFO("lidar incre :%f  %f  %f  len:%f  att :%f   %f  %f\n",x ,  y,  z,  len , roll ,  pitch,  yaw);
	    ROS_INFO("lidar front :%f  %f  %f  len:%f  att :%f   %f  %f\n",x0 , y0, z0, len0, roll0 , pitch0, yaw0);
	    ROS_INFO("lidar back  :%f  %f  %f  len:%f  att :%f   %f  %f\n",x1 , y1, z1, len1, roll1 , pitch1, yaw1);		

        laserOdomIncremental.header.stamp = ros::Time().fromSec( laserTime );
        laserOdomIncremental.header.frame_id = "odom_mapping";
           // laserOdomIncremental.child_frame_id = "odom_mapping";
        laserOdomIncremental.pose.pose.position.x = x;
        laserOdomIncremental.pose.pose.position.y = y;
        laserOdomIncremental.pose.pose.position.z = z;
        laserOdomIncremental.pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);

        pubLaserOdometryIncremental.publish(laserOdomIncremental);
    }


void process()
{
	while(1)
	{
		while (!cornerLastBuf.empty() && !surfLastBuf.empty() &&
			!fullResBuf.empty() && !odometryBuf.empty())
		{

			mBuf.lock();
			while (!odometryBuf.empty() && odometryBuf.front()->header.stamp.toSec() < cornerLastBuf.front()->header.stamp.toSec())
				odometryBuf.pop();
			if (odometryBuf.empty())
			{
				mBuf.unlock();
				break;
			}

			while (!surfLastBuf.empty() && surfLastBuf.front()->header.stamp.toSec() < cornerLastBuf.front()->header.stamp.toSec())
				surfLastBuf.pop();
			if (surfLastBuf.empty())
			{
				mBuf.unlock();
				break;
			}

			while (!fullResBuf.empty() && fullResBuf.front()->header.stamp.toSec() < cornerLastBuf.front()->header.stamp.toSec())
				fullResBuf.pop();
			if (fullResBuf.empty())
			{
				mBuf.unlock();
				break;
			}

			timeLaserCloudCornerLast = cornerLastBuf.front()->header.stamp.toSec();
			timeLaserCloudSurfLast = surfLastBuf.front()->header.stamp.toSec();
			timeLaserCloudFullRes = fullResBuf.front()->header.stamp.toSec();
			timeLaserOdometry = odometryBuf.front()->header.stamp.toSec();

			if (timeLaserCloudCornerLast != timeLaserOdometry ||
				timeLaserCloudSurfLast != timeLaserOdometry ||
				timeLaserCloudFullRes != timeLaserOdometry)
			{
				ROS_INFO("time corner %f surf %f full %f odom %f \n", timeLaserCloudCornerLast, timeLaserCloudSurfLast, timeLaserCloudFullRes, timeLaserOdometry);
				ROS_INFO("unsync messeage!");
				mBuf.unlock();
				break;
			}
			laserCloudCornerLast->clear();
			pcl::fromROSMsg(*cornerLastBuf.front(), *laserCloudCornerLast);
			cornerLastBuf.pop();

			laserCloudSurfLast->clear();
			pcl::fromROSMsg(*surfLastBuf.front(), *laserCloudSurfLast);
			surfLastBuf.pop();

			laserCloudFullRes->clear();
			pcl::fromROSMsg(*fullResBuf.front(), *laserCloudFullRes);
			fullResBuf.pop();
          
            //change localmap 
			CornerCloudMsgBuf.insert(std::make_pair(timeLaserCloudCornerLast, *laserCloudCornerLast));
			SurfCloudMsgBuf.insert(std::make_pair(timeLaserCloudSurfLast, *laserCloudSurfLast));
		//	FullCloudMsgBuf.insert(std::make_pair(timeLaserCloudFullRes, *laserCloudFullRes));
            //save keyframe 
			TicToc  keyframTime;
		    m_current_frame_index ++;

			q_wodom_curr.x() = odometryBuf.front()->pose.pose.orientation.x;
			q_wodom_curr.y() = odometryBuf.front()->pose.pose.orientation.y;
			q_wodom_curr.z() = odometryBuf.front()->pose.pose.orientation.z;
			q_wodom_curr.w() = odometryBuf.front()->pose.pose.orientation.w;
			t_wodom_curr.x() = odometryBuf.front()->pose.pose.position.x;
			t_wodom_curr.y() = odometryBuf.front()->pose.pose.position.y;
			t_wodom_curr.z() = odometryBuf.front()->pose.pose.position.z;
			odometryBuf.pop();
            
			while(!cornerLastBuf.empty())
			{
				cornerLastBuf.pop();
				ROS_INFO("drop lidar frame in mapping for real time performance \n");
			}

			mBuf.unlock();
            		
			TicToc t_whole;
			transformAssociateToMap();
			laserCloudCornerFromMap->clear();
			laserCloudSurfFromMap->clear();

            //define the localmap  pose range 
			Eigen::Vector3d t_curr(t_w_curr.x(), t_w_curr.y(), t_w_curr.z());
            LocalMapPoseFix(timeLaserOdometry,t_curr);
		    UpdateLocalMap( CornerCloudMsgBuf, laserCloudCornerFromMap,downSizeFilterCorner );
	        UpdateLocalMap( SurfCloudMsgBuf, laserCloudSurfFromMap, downSizeFilterSurf);

			static int mapcloudnum = 0;

			int laserCloudCornerFromMapNum = laserCloudCornerFromMap->points.size();
			int laserCloudSurfFromMapNum = laserCloudSurfFromMap->points.size();

			pcl::PointCloud<PointType>::Ptr laserCloudCornerStack(new pcl::PointCloud<PointType>());
			downSizeFilterCorner.setInputCloud(laserCloudCornerLast);
			downSizeFilterCorner.filter(*laserCloudCornerStack);
			int laserCloudCornerStackNum = laserCloudCornerStack->points.size();

			pcl::PointCloud<PointType>::Ptr laserCloudSurfStack(new pcl::PointCloud<PointType>());
			downSizeFilterSurf.setInputCloud(laserCloudSurfLast);
			downSizeFilterSurf.filter(*laserCloudSurfStack);
			int laserCloudSurfStackNum = laserCloudSurfStack->points.size();

			if (laserCloudCornerFromMapNum > 10 && laserCloudSurfFromMapNum > 50)
			{
				TicToc t_opt;
				TicToc t_tree;
				kdtreeCornerFromMap->setInputCloud(laserCloudCornerFromMap);
				kdtreeSurfFromMap->setInputCloud(laserCloudSurfFromMap);

				for (int iterCount = 0; iterCount < 2; iterCount++)
				{
					//ceres::LossFunction *loss_function = NULL;
					ceres::LossFunction *loss_function = new ceres::HuberLoss(0.1);
					ceres::LocalParameterization *q_parameterization =
						new ceres::EigenQuaternionParameterization();
					ceres::Problem::Options problem_options;

					ceres::Problem problem(problem_options);
					problem.AddParameterBlock(parameters, 4, q_parameterization);
					problem.AddParameterBlock(parameters + 4, 3);

					ceres::ResidualBlockId              block_id;
                    std::vector<ceres::ResidualBlockId> residual_block_ids;

					TicToc t_data;
					int corner_num = 0;
					for (int i = 0; i < laserCloudCornerStackNum; i++)
					{
						pointOri = laserCloudCornerStack->points[i];
						double sqrtDis = pointOri.x * pointOri.x + pointOri.y * pointOri.y + pointOri.z * pointOri.z;
						pointAssociateToMap(&pointOri, &pointSel);
						kdtreeCornerFromMap->nearestKSearch(pointSel, 5, pointSearchInd, pointSearchSqDis); 

						if (pointSearchSqDis[4] < 1.0)
						{ 
							std::vector<Eigen::Vector3d> nearCorners;
							Eigen::Vector3d center(0, 0, 0);
							for (int j = 0; j < 5; j++)
							{
								Eigen::Vector3d tmp(laserCloudCornerFromMap->points[pointSearchInd[j]].x,
													laserCloudCornerFromMap->points[pointSearchInd[j]].y,
													laserCloudCornerFromMap->points[pointSearchInd[j]].z);
								center = center + tmp;
								nearCorners.push_back(tmp);
							}
							center = center / 5.0;

							Eigen::Matrix3d covMat = Eigen::Matrix3d::Zero();
							for (int j = 0; j < 5; j++)
							{
								Eigen::Matrix<double, 3, 1> tmpZeroMean = nearCorners[j] - center;
								covMat = covMat + tmpZeroMean * tmpZeroMean.transpose();
							}

							Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> saes(covMat);
							// if is indeed line feature
							// note Eigen library sort eigenvalues in increasing order
							Eigen::Vector3d unit_direction = saes.eigenvectors().col(2);
							Eigen::Vector3d curr_point(pointOri.x, pointOri.y, pointOri.z);
							if (saes.eigenvalues()[2] > 3 * saes.eigenvalues()[1])
							{ 
								Eigen::Vector3d point_on_line = center;
								Eigen::Vector3d point_a, point_b;
								point_a = 0.1 * unit_direction + point_on_line;
								point_b = -0.1 * unit_direction + point_on_line;

								ceres::CostFunction *cost_function = LidarEdgeFactor::Create(curr_point, point_a, point_b, 1.0);
								block_id=problem.AddResidualBlock(cost_function, loss_function, parameters, parameters + 4);
								residual_block_ids.push_back(block_id);
								corner_num++;	
							}							
						}	
						else if(pointSearchSqDis[4] < 0.01 * sqrtDis)
						{
							Eigen::Vector3d center(0, 0, 0);
							for (int j = 0; j < 5; j++)
							{
								Eigen::Vector3d tmp(laserCloudCornerFromMap->points[pointSearchInd[j]].x,
													laserCloudCornerFromMap->points[pointSearchInd[j]].y,
													laserCloudCornerFromMap->points[pointSearchInd[j]].z);
								center = center + tmp;
							}
							center = center / 5.0;	
							Eigen::Vector3d curr_point(pointOri.x, pointOri.y, pointOri.z);
							ceres::CostFunction *cost_function = LidarDistanceFactor::Create(curr_point, center);
							problem.AddResidualBlock(cost_function, loss_function, parameters, parameters + 4);
						}						
					}

					int surf_num = 0;
					for (int i = 0; i < laserCloudSurfStackNum; i++)
					{
						pointOri = laserCloudSurfStack->points[i];
						double sqrtDis = pointOri.x * pointOri.x + pointOri.y * pointOri.y + pointOri.z * pointOri.z;
						pointAssociateToMap(&pointOri, &pointSel);
						kdtreeSurfFromMap->nearestKSearch(pointSel, 5, pointSearchInd, pointSearchSqDis);

						Eigen::Matrix<double, 5, 3> matA0;
						Eigen::Matrix<double, 5, 1> matB0 = -1 * Eigen::Matrix<double, 5, 1>::Ones();
						if (pointSearchSqDis[4] < 1.0)
						{
							for (int j = 0; j < 5; j++)
							{
								matA0(j, 0) = laserCloudSurfFromMap->points[pointSearchInd[j]].x;
								matA0(j, 1) = laserCloudSurfFromMap->points[pointSearchInd[j]].y;
								matA0(j, 2) = laserCloudSurfFromMap->points[pointSearchInd[j]].z;
								//printf(" pts %f %f %f ", matA0(j, 0), matA0(j, 1), matA0(j, 2));
							}
							// find the norm of plane
							Eigen::Vector3d norm = matA0.colPivHouseholderQr().solve(matB0);
							double negative_OA_dot_norm = 1 / norm.norm();
							norm.normalize();

							// Here n(pa, pb, pc) is unit norm of plane
							bool planeValid = true;
							for (int j = 0; j < 5; j++)
							{
								// if OX * n > 0.2, then plane is not fit well
								if (fabs(norm(0) * laserCloudSurfFromMap->points[pointSearchInd[j]].x +
										 norm(1) * laserCloudSurfFromMap->points[pointSearchInd[j]].y +
										 norm(2) * laserCloudSurfFromMap->points[pointSearchInd[j]].z + negative_OA_dot_norm) > 0.2)
								{
									planeValid = false;
									break;
								}
							}
							Eigen::Vector3d curr_point(pointOri.x, pointOri.y, pointOri.z);
							if (planeValid)
							{
								ceres::CostFunction *cost_function = LidarPlaneNormFactor::Create(curr_point, norm, negative_OA_dot_norm);
								block_id = problem.AddResidualBlock(cost_function, loss_function, parameters, parameters + 4);
								residual_block_ids.push_back(block_id);
								surf_num++;
							}
						}
						else if(pointSearchSqDis[4] < 0.01 * sqrtDis)
						{
							Eigen::Vector3d center(0, 0, 0);
							for (int j = 0; j < 5; j++)
							{
								Eigen::Vector3d tmp(laserCloudSurfFromMap->points[pointSearchInd[j]].x,
													laserCloudSurfFromMap->points[pointSearchInd[j]].y,
													laserCloudSurfFromMap->points[pointSearchInd[j]].z);
								center = center + tmp;
							}
							center = center / 5.0;	
							Eigen::Vector3d curr_point(pointOri.x, pointOri.y, pointOri.z);
							ceres::CostFunction *cost_function = LidarDistanceFactor::Create(curr_point, center);
							problem.AddResidualBlock(cost_function, loss_function, parameters, parameters + 4);
						}
					}

					TicToc t_solver;
					ceres::Solver::Options options;
					options.linear_solver_type = ceres::DENSE_QR;
					options.max_num_iterations = 4;
					options.minimizer_progress_to_stdout = false;
					options.check_gradients = false;
					options.gradient_check_relative_precision = 1e-4;
					ceres::Solver::Summary summary;
					ceres::Solve(options, &problem, &summary);
#if 0
                    std::vector< ceres::ResidualBlockId> residual_block_ids_temp;
                    residual_block_ids_temp.resize(residual_block_ids.size());
                    residual_block_ids_temp.clear();
                    ceres::Problem::EvaluateOptions eval_options;
                    eval_options.residual_blocks = residual_block_ids;
                    double  total_cos = 0.0;
                    std::vector<double> residuals;
                    problem.Evaluate(eval_options,&total_cost,&residuals,nullptr,nullptr);
                    
                    double m_inliner_ration_threshold = compute_inlier_residual_threshold(residuals,m_inlier_ratio);
                    m_inlier_threshold =  std::max(m_inliner_dis,m_inliner_ration_threshold);
                    int remove_residuals_num =0;
                    for(unsigned int i=0; i< residual_block_ids.size(); i++)
                    {
                        if( (fabs( residuals[ 3*i + 0 ])+ fabs( residuals[ 3*i + 1 ])+fabs( residuals[ 3*i + 2 ])) > m_inlier_threshold)
                        {                               
                            problem.RemoveResidualBlock(residual_block_ids[i]);
                            remove_residuals_num++;
                        }
                        else
                        {
                            residual_block_ids_temp.push_back( residual_block_ids[i]);
                        }
                    }   
                    residual_block_ids = residual_block_ids_temp;
                    ceres::Solve( options, &problem, &summary );
#endif

				}
		//		printf("mapping optimization time %f \n", t_opt.toc());
			}
			else
			{
				ROS_WARN("time Map corner and surf num are not enough");
			}
			transformUpdate();
			TicToc t_pub;
			//publish surround map for every 5 frame
			if (frameCount % 5 == 0)
			{
				laserCloudSurround->clear();
				*laserCloudSurround += *laserCloudCornerFromMap;
				*laserCloudSurround += *laserCloudSurfFromMap;
				sensor_msgs::PointCloud2 laserCloudSurround3;
				pcl::toROSMsg(*laserCloudSurround, laserCloudSurround3);
				laserCloudSurround3.header.stamp = ros::Time().fromSec(timeLaserOdometry);
				laserCloudSurround3.header.frame_id = "camera_init";
				pubLaserCloudSurround.publish(laserCloudSurround3);
			}
			printf("^^^^^^^^^^^^^^^frameCount:%d\n",frameCount);
			if (frameCount % 20 == 0)
			{
				pcl::PointCloud<PointType> laserCloudMap;
				laserCloudMap += *laserCloudCornerFromMap;
				laserCloudMap += *laserCloudSurfFromMap;
				sensor_msgs::PointCloud2 laserCloudMsg;
				pcl::toROSMsg(laserCloudMap, laserCloudMsg);
				printf("^^^^^^^^^^^^^^^laserCloudMap size:%d\n",laserCloudMap.size());
				laserCloudMsg.header.stamp = ros::Time().fromSec(timeLaserOdometry);
				laserCloudMsg.header.frame_id = "camera_init";
				pubLaserCloudMap.publish(laserCloudMsg);
			}

			/*  delete full point cloud transform
				int laserCloudFullResNum = laserCloudFullRes->points.size();
				for (int i = 0; i < laserCloudFullResNum; i++)
				{
					pointAssociateToMap(&laserCloudFullRes->points[i], &laserCloudFullRes->points[i]);
				}
				
				sensor_msgs::PointCloud2 laserCloudFullRes3;
				pcl::toROSMsg(*laserCloudFullRes, laserCloudFullRes3);
				laserCloudFullRes3.header.stamp = ros::Time().fromSec(timeLaserOdometry);
				laserCloudFullRes3.header.frame_id = "camera_init";
				pubLaserCloudFullRes.publish(laserCloudFullRes3);
			*/

			//save key poses
			PointType thisPose3D;
			PointTypePose thisPose6D;

			Eigen::Vector3d latest_att = q2att(q_w_curr);//roll pitch yaw
			thisPose3D.x = t_w_curr.x();
			thisPose3D.y = t_w_curr.y();
			thisPose3D.z = t_w_curr.z();
			thisPose3D.intensity = cloudKeyPoses3D->size(); // this can be used as index
			cloudKeyPoses3D->push_back(thisPose3D);

			thisPose6D.x = thisPose3D.x;
			thisPose6D.y = thisPose3D.y;
			thisPose6D.z = thisPose3D.z;
			thisPose6D.intensity = thisPose3D.intensity ; // this can be used as index
			thisPose6D.roll  = latest_att.x();
			thisPose6D.pitch = latest_att.y();
			thisPose6D.yaw   = latest_att.z();
			thisPose6D.time = timeLaserOdometry;
			cloudKeyPoses6D->push_back(thisPose6D);

			// save updated transform
			float lastpos[6];
			lastpos[0] = latest_att.x();
			lastpos[1] = latest_att.y();
			lastpos[2] = latest_att.z();
			lastpos[3] = t_w_curr.x();
			lastpos[4] = t_w_curr.y();
			lastpos[5] = t_w_curr.z();

			incrementalOdometryAffineBack = pcl::getTransformation(lastpos[3], lastpos[4],
																lastpos[5], lastpos[0], lastpos[1], lastpos[2]);
			publishIncrementalOdometry(lastpos, timeLaserOdometry);
			printf("increment back :%f  %f  %f \n",lastpos[3],lastpos[4],lastpos[5]);
			t_w_curr[0] = lastpos[3];
			t_w_curr[1] = lastpos[4];
			t_w_curr[2] = lastpos[5];	
			Eigen::Quaterniond qt = a2qua(lastpos[1], lastpos[0] ,lastpos[2]);
			q_w_curr = qt;	 
			//	printf("mapping pub time %f ms \n", t_pub.toc());
           
		//	printf("whole mapping time %f ms +++++\n", t_whole.toc());
			nav_msgs::Odometry odomAftMapped;
			odomAftMapped.header.frame_id = "camera_init";
			odomAftMapped.child_frame_id = "aft_mapped";
			odomAftMapped.header.stamp = ros::Time().fromSec(timeLaserOdometry);
			odomAftMapped.pose.pose.orientation.x = q_w_curr.x();
			odomAftMapped.pose.pose.orientation.y = q_w_curr.y();
			odomAftMapped.pose.pose.orientation.z = q_w_curr.z();
			odomAftMapped.pose.pose.orientation.w = q_w_curr.w();
			odomAftMapped.pose.pose.position.x = t_w_curr.x();
			odomAftMapped.pose.pose.position.y = t_w_curr.y();
			odomAftMapped.pose.pose.position.z = t_w_curr.z();
			pubOdomAftMapped.publish(odomAftMapped);

			geometry_msgs::PoseStamped laserAfterMappedPose;
			laserAfterMappedPose.header = odomAftMapped.header;
			laserAfterMappedPose.pose = odomAftMapped.pose.pose;

			mOptmization.lock();
			posestapedBuf.push_back(laserAfterMappedPose);
			laserAfterMappedPath.header.stamp = odomAftMapped.header.stamp;
			laserAfterMappedPath.header.frame_id = "camera_init";
			laserAfterMappedPath.poses.push_back(laserAfterMappedPose);
			pubLaserAfterMappedPath.publish(laserAfterMappedPath);
            mOptmization.unlock();

			static tf::TransformBroadcaster br;
			tf::Transform transform;
			tf::Quaternion q;
			transform.setOrigin(tf::Vector3(t_w_curr(0),
											t_w_curr(1),
											t_w_curr(2)));
			q.setW(q_w_curr.w());
			q.setX(q_w_curr.x());
			q.setY(q_w_curr.y());
			q.setZ(q_w_curr.z());
			transform.setRotation(q);
			br.sendTransform(tf::StampedTransform(transform, odomAftMapped.header.stamp, "camera_init", "aft_mapped"));

			frameCount++;
		}
		std::chrono::milliseconds dura(2);
        std::this_thread::sleep_for(dura);
	}
}

int main(int argc, char **argv)
{

	ros::init(argc, argv, "laserMapping");
	ros::NodeHandle nh;

	float lineRes = 0;
	float planeRes = 0;
	nh.param<float>("mapping_line_resolution", lineRes, 0.4);
	nh.param<float>("mapping_plane_resolution", planeRes, 0.8);
	ROS_INFO("line resolution %f plane resolution %f \n", lineRes, planeRes);
	downSizeFilterCorner.setLeafSize(lineRes, lineRes,lineRes);
	downSizeFilterSurf.setLeafSize(planeRes, planeRes, planeRes);
    
	incrementalOdometryAffineBack = pcl::getTransformation(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	incrementalOdometryAffineFront = pcl::getTransformation(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

    cloudKeyPoses3D.reset(new pcl::PointCloud<PointType>());
    cloudKeyPoses6D.reset(new pcl::PointCloud<PointTypePose>());
    copy_cloudKeyPoses3D.reset(new pcl::PointCloud<PointType>());


	ros::Subscriber subLaserCloudCornerLast = nh.subscribe<sensor_msgs::PointCloud2>("/laser_cloud_corner_last", 100, laserCloudCornerLastHandler);

	ros::Subscriber subLaserCloudSurfLast = nh.subscribe<sensor_msgs::PointCloud2>("/laser_cloud_surf_last", 100, laserCloudSurfLastHandler);

	ros::Subscriber subLaserOdometry = nh.subscribe<nav_msgs::Odometry>("/laser_odom_to_init", 100, laserOdometryHandler);

	ros::Subscriber subLaserCloudFullRes = nh.subscribe<sensor_msgs::PointCloud2>("/velodyne_cloud_3", 100, laserCloudFullResHandler);

    // get lidar's before matching's pose
	ros::Subscriber subLaserBeforeOdometry= nh.subscribe<nav_msgs::Odometry>("/before_laser_odom",10 ,getLaserBeforeOdometry);	
    
	pubLaserCloudSurround = nh.advertise<sensor_msgs::PointCloud2>("/laser_cloud_surround", 100);

	pubLaserCloudMap = nh.advertise<sensor_msgs::PointCloud2>("/laser_cloud_map", 100);

	pubLaserCloudFullRes = nh.advertise<sensor_msgs::PointCloud2>("/velodyne_cloud_registered", 100);

	pubOdomAftMapped = nh.advertise<nav_msgs::Odometry>("/aft_mapped_to_init", 100);

	pubOdomAftMappedHighFrec = nh.advertise<nav_msgs::Odometry>("/aft_mapped_to_init_high_frec", 100);

	pubLaserAfterMappedPath = nh.advertise<nav_msgs::Path>("/aft_mapped_path", 100);

    pubLaserOdometryIncremental = nh.advertise<nav_msgs::Odometry> ("lio_sam/mapping/odometry_incremental", 1);

	std::thread mapping_process{process};


	ros::spin();

	return 0;
}