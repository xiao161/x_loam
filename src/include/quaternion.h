
#include "eigen3/Eigen/Dense"

Eigen::Quaterniond a2qua(double pitch, double roll, double yaw);
Eigen::Vector3d  q2att(const Eigen::Quaterniond &q);
Eigen::Matrix3d q2mat(const Eigen::Quaterniond &qnb_);
Eigen::Quaterniond m2qua(const Eigen::Matrix3d  Cnb);

