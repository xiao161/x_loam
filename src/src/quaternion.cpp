#include "quaternion.h"
//pitch roll yaw
Eigen::Quaterniond a2qua(double pitch, double roll, double yaw)
{
	pitch /= 2.0, roll /= 2.0, yaw /= 2.0;
    double	sp = sin(pitch), sr = sin(roll), sy = sin(yaw), 
			cp = cos(pitch), cr = cos(roll), cy = cos(yaw);
	Eigen::Vector4d qnb;

    qnb[0] = cp*cr*cy - sp*sr*sy;//w
    qnb[1] = sp*cr*cy - cp*sr*sy;//x
    qnb[2] = cp*sr*cy + sp*cr*sy;//y
    qnb[3] = cp*cr*sy + sp*sr*cy;//z
    Eigen::Quaterniond rq(qnb[0],qnb[1],qnb[2],qnb[3]);//w,x,y,z
	return rq;
}
//get pitch roll yaw
Eigen::Vector3d  q2att(const Eigen::Quaterniond &q)
{
    Eigen::Vector4d qnb(q.w(),q.x(),q.y(),q.z());//w,x,y,z
	double	q11 = qnb[0]*qnb[0], q12 = qnb[0]*qnb[1], q13 = qnb[0]*qnb[2], q14 = qnb[0]*qnb[3], 
			q22 = qnb[1]*qnb[1], q23 = qnb[1]*qnb[2], q24 = qnb[1]*qnb[3],     
			q33 = qnb[2]*qnb[2], q34 = qnb[2]*qnb[3],  
			q44 = qnb[3]*qnb[3];
	Eigen::Vector3d  att;
	att[0] = asin(2*(q34+q12));
	att[1] = atan2(-2*(q24-q13), q11-q22-q33+q44);
	att[2]= atan2(-2*(q23-q14), q11-q22+q33-q44);
	return att;
}
Eigen::Matrix3d q2mat(const Eigen::Quaterniond &qnb_)
{
	Eigen::Vector4d qnb(qnb_.w(),qnb_.x(),qnb_.y(),qnb_.z());
	double	q11 = qnb[0]*qnb[0], q12 = qnb[0]*qnb[1], q13 = qnb[0]*qnb[2], q14 = qnb[0]*qnb[3], 
			q22 = qnb[1]*qnb[1], q23 = qnb[1]*qnb[2], q24 = qnb[1]*qnb[3],     
			q33 = qnb[2]*qnb[2], q34 = qnb[2]*qnb[3],  
			q44 = qnb[3]*qnb[3];
	Eigen::Matrix3d Cnb;
    Cnb<<  q11+q22-q33-q44, 2*(q23-q14), 2*(q24+q13),
		2*(q23+q14),       q11-q22+q33-q44,  2*(q34-q12),
		2*(q24-q13),       2*(q34+q12),      q11-q22-q33+q44;
	return Cnb;
}
Eigen::Quaterniond m2qua(const Eigen::Matrix3d  Cnb)
{
	double q0, q1, q2, q3, qq4;
    if(Cnb(0,0)>=Cnb(1,1)+Cnb(2,2))
	{
        q1 = 0.5*sqrt(1+Cnb(0,0)-Cnb(1,1)-Cnb(2,2)); 
		qq4 = 4*q1;
        q0 = (Cnb(2,1)-Cnb(1,2))/qq4;
		q2 = (Cnb(0,1)+Cnb(1,0))/qq4;
		q3 = (Cnb(0,2)+Cnb(2,0))/qq4;
	}
    else if(Cnb(1,1)>=Cnb(0,0)+Cnb(2,2))
	{
        q2 = 0.5*sqrt(1-Cnb(0,0)+Cnb(1,1)-Cnb(2,2));
		qq4 = 4*q2;
        q0 = (Cnb(0,2)-Cnb(2,0))/qq4; 
		q1 = (Cnb(0,1)+Cnb(1,0))/qq4;
		q3 = (Cnb(1,2)+Cnb(2,1))/qq4;
	}
    else if(Cnb(2,2)>=Cnb(0,0)+Cnb(1,1))
	{
        q3 = 0.5*sqrt(1-Cnb(0,0)-Cnb(1,1)+Cnb(2,2));
		qq4 = 4*q3;
        q0 = (Cnb(1,0)-Cnb(0,1))/qq4;
		q1 = (Cnb(0,2)+Cnb(2,0))/qq4;
		q2 = (Cnb(1,2)+Cnb(2,1))/qq4;
	}
    else
	{
        q0 = 0.5*sqrt(1+Cnb(0,0)+Cnb(1,1)+Cnb(2,2));
		qq4 = 4*q0;
        q1 = (Cnb(2,1)-Cnb(1,2))/qq4;
		q2 = (Cnb(0,2)-Cnb(2,0))/qq4;
		q3 = (Cnb(1,0)-Cnb(0,1))/qq4;
	}
	double nq = sqrt(q0*q0+q1*q1+q2*q2+q3*q3);
	q0 /= nq; q1 /= nq; q2 /= nq; q3 /= nq;
	Eigen::Quaterniond q(q0, q1, q2, q3);
	return q;
}