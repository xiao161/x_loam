// This is an advanced implementation of the algorithm described in the following paper:
//   J. Zhang and S. Singh. LOAM: Lidar Odometry and Mapping in Real-time.
//     Robotics: Science and Systems Conference (RSS). Berkeley, CA, July 2014. 

// Modifier: Tong Qin               qintonguav@gmail.com
// 	         Shaozu Cao 		    saozu.cao@connect.ust.hk


// Copyright 2013, Ji Zhang, Carnegie Mellon University
// Further contributions copyright (c) 2016, Southwest Research Institute
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <cmath>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl_conversions/pcl_conversions.h>
#include <ros/ros.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl/search/impl/search.hpp>
#include <pcl/range_image/range_image.h>

#include <pcl/common/common.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/icp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/crop_box.h> 

#include <tf/LinearMath/Quaternion.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>

#include <eigen3/Eigen/Dense>
#include <mutex>
#include <queue>
#include <algorithm>

#include "tic_toc.h"
#include "quaternion.h"
#include "lidarFactor.hpp"

#define DISTORTION 0

int corner_correspondence = 0, plane_correspondence = 0;

constexpr double SCAN_PERIOD = 0.1;
constexpr double DISTANCE_SQ_THRESHOLD = 25;
constexpr double NEARBY_SCAN = 2.5;

int skipFrameNum = 5;
bool systemInited = false;

double timeCornerPointsSharp = 0;
double timeCornerPointsLessSharp = 0;
double timeSurfPointsFlat = 0;
double timeSurfPointsLessFlat = 0;
double timeLaserCloudFullRes = 0;

//livox
double m_inlier_threshold;
int    m_maximum_allow_residual_block = 1e5;
double m_inlier_ratio = 0.80;
double m_inliner_dis = 0.02;
double m_residuals_len_threshold=0.1;

typedef pcl::PointXYZI PointType;

pcl::KdTreeFLANN<pcl::PointXYZI>::Ptr kdtreeCornerLast(new pcl::KdTreeFLANN<pcl::PointXYZI>());
pcl::KdTreeFLANN<pcl::PointXYZI>::Ptr kdtreeSurfLast(new pcl::KdTreeFLANN<pcl::PointXYZI>());

pcl::PointCloud<PointType>::Ptr cornerPointsSharp(new pcl::PointCloud<PointType>());
pcl::PointCloud<PointType>::Ptr cornerPointsLessSharp(new pcl::PointCloud<PointType>());
pcl::PointCloud<PointType>::Ptr surfPointsFlat(new pcl::PointCloud<PointType>());
pcl::PointCloud<PointType>::Ptr surfPointsLessFlat(new pcl::PointCloud<PointType>());

pcl::PointCloud<PointType>::Ptr laserCloudCornerLast(new pcl::PointCloud<PointType>());
pcl::PointCloud<PointType>::Ptr laserCloudSurfLast(new pcl::PointCloud<PointType>());
pcl::PointCloud<PointType>::Ptr laserCloudFullRes(new pcl::PointCloud<PointType>());

int laserCloudCornerLastNum = 0;
int laserCloudSurfLastNum = 0;

// Transformation from current frame to world frame
Eigen::Quaterniond q_w_curr(1, 0, 0, 0);
Eigen::Vector3d t_w_curr(0, 0, 0);

// q_curr_last(x, y, z, w), t_curr_last
double para_q[4] = {0, 0, 0, 1};
double para_t[3] = {0, 0, 0};

Eigen::Map<Eigen::Quaterniond> q_last_curr(para_q);
Eigen::Map<Eigen::Vector3d> t_last_curr(para_t);

std::queue<sensor_msgs::PointCloud2ConstPtr> cornerSharpBuf;
std::queue<sensor_msgs::PointCloud2ConstPtr> cornerLessSharpBuf;
std::queue<sensor_msgs::PointCloud2ConstPtr> surfFlatBuf;
std::queue<sensor_msgs::PointCloud2ConstPtr> surfLessFlatBuf;
std::queue<sensor_msgs::PointCloud2ConstPtr> fullPointsBuf;

std::queue<nav_msgs::Odometry::ConstPtr> afterMapPoseBuf;

std::mutex mBuf;
std::mutex mAfterMapPoseBuf;

//remove residual of the pointCloud
enum Feature_type
{
    e_surfPointsFlat = 0,
    e_surfPointsLessFlat = 1,
    e_cornerPointsLessSharp = 2,
    e_cornerPointsSharp = 3
};
struct PointFeature{
    PointType point;
    Feature_type m_feature_type;
    int removeflag ;
};
float RemoveSquare = 3.0;//the remove residual square's length
int rowx =0, columny = 0, highz = 0; //square's size
float maxx = 0.0, minx = 0.0, maxy = 0.0, miny = 0.0, minz = 0.0, maxz = 0.0; //square's  min or max xyz
size_t minFlatCloudSize = 40, minLessFlatCloudSize = 2000, minSharpCloudSize = 80, minLessSharpCloudSize = 500;

std::mutex preImuOdoLock;
double initialGuessX = 0.0, initialGuessY = 0.0, initialGuessZ = 0.0, initialGuessRoll = 0.0, initialGuessPitch = 0.0, initialGuessYaw = 0.0;
bool preImuInitalFlag = false;

float lastLaserPose[6] = {0.0};
float preTreatment[6] = {0.0};

// undistort lidar point
void TransformToStart(PointType const *const pi, PointType *const po)
{
    //interpolation ratio
    double s;
    if (DISTORTION)
        s = (pi->intensity - int(pi->intensity)) / SCAN_PERIOD;
    else
        s = 1.0;
    //s = 1;
    Eigen::Quaterniond q_point_last = Eigen::Quaterniond::Identity().slerp(s, q_last_curr);
    Eigen::Vector3d t_point_last = s * t_last_curr;
    Eigen::Vector3d point(pi->x, pi->y, pi->z);
    Eigen::Vector3d un_point = q_point_last * point + t_point_last;

    po->x = un_point.x();
    po->y = un_point.y();
    po->z = un_point.z();
    po->intensity = pi->intensity;
}

// transform all lidar points to the start of the next frame
void TransformToEnd(PointType const *const pi, PointType *const po)
{
    // undistort point first
    pcl::PointXYZI un_point_tmp;
    TransformToStart(pi, &un_point_tmp);

    Eigen::Vector3d un_point(un_point_tmp.x, un_point_tmp.y, un_point_tmp.z);
    Eigen::Vector3d point_end = q_last_curr.inverse() * (un_point - t_last_curr);

    po->x = point_end.x();
    po->y = point_end.y();
    po->z = point_end.z();

    //Remove distortion time info
    po->intensity = int(pi->intensity);
}

void laserCloudSharpHandler(const sensor_msgs::PointCloud2ConstPtr &cornerPointsSharp2)
{
    mBuf.lock();
    cornerSharpBuf.push(cornerPointsSharp2);
    mBuf.unlock();
}

void laserCloudLessSharpHandler(const sensor_msgs::PointCloud2ConstPtr &cornerPointsLessSharp2)
{

    mBuf.lock();
    cornerLessSharpBuf.push(cornerPointsLessSharp2);
    mBuf.unlock();
}

void laserCloudFlatHandler(const sensor_msgs::PointCloud2ConstPtr &surfPointsFlat2)
{

    mBuf.lock();
    surfFlatBuf.push(surfPointsFlat2);
    mBuf.unlock();
}

void laserCloudLessFlatHandler(const sensor_msgs::PointCloud2ConstPtr &surfPointsLessFlat2)
{
    mBuf.lock();
    surfLessFlatBuf.push(surfPointsLessFlat2);
    mBuf.unlock();
}

//receive all point cloud
void laserCloudFullResHandler(const sensor_msgs::PointCloud2ConstPtr &laserCloudFullRes2)
{
    mBuf.lock();
    fullPointsBuf.push(laserCloudFullRes2);
    mBuf.unlock();
}
//livox
 double compute_inlier_residual_threshold( std::vector< double > residuals, double ratio )
  {
      std::set< double > dis_vec;
      for ( size_t i = 0; i < ( size_t )( residuals.size() / 3 ); i++ )
      {
          dis_vec.insert( fabs( residuals[ 3 * i + 0 ] ) + fabs( residuals[ 3 * i + 1 ] ) + fabs( residuals[ 3 * i + 2 ] ) );
      }
      return *( std::next( dis_vec.begin(), ( int ) ( ( ratio ) * dis_vec.size() ) ) );
  }
void remove_residual_feature_points_openmp(pcl::PointCloud<PointType>::Ptr cloud,std::vector<pcl::PointXYZI> residuals_ids,int numThreshold = 0, float para = 1.0)
{
    pcl::KdTreeFLANN<PointType>::Ptr kdtreeTempFromMap(new pcl::KdTreeFLANN<PointType>());
    kdtreeTempFromMap->setInputCloud( cloud );
    TicToc timec;
#pragma omp parallel for
    for(size_t i = 0; i < cloud->points.size() ; i++)
    {
        PointType point=cloud->points[i];
        int pointNum = 5;
        std::vector<int> vectorIndx( pointNum );
        std::vector<float> vectorDistance( pointNum );
        if( kdtreeTempFromMap->nearestKSearch(point, pointNum, vectorIndx, vectorDistance) > 0 )
        {           
            for(int j = 0; j < pointNum; j++ )
            {
                if(sqrt( vectorDistance[j] ) < m_residuals_len_threshold / para)
                {
                    auto iterTemp = cloud->begin() + vectorIndx[j];
                    cloud->erase( iterTemp ); 
                }
            }
        }
    }
    std::cout<<"time:"<<timec.toc()<<std::endl;
}
void remove_residual_feature_points(pcl::PointCloud<PointType>::Ptr cloud,std::vector<pcl::PointXYZI> residuals_ids,int numThreshold = 0, float para = 1.0)
{
    pcl::PointCloud<PointType>::Ptr cloudTemp(new pcl::PointCloud<PointType>());
    pcl::PointCloud<PointType>::Ptr cloudAfter(new pcl::PointCloud<PointType>());
    ( *cloudTemp ) += ( *cloud );
    cloud->clear();
    for(size_t i = 0;i < cloudTemp->points.size() ; i++)
    {
        PointType point = cloudTemp->points[i];
        pcl::PointXYZI residual_points;
        char flag = 0;
        for(size_t j = 0; j< residuals_ids.size() ; j++)
        {
          residual_points = residuals_ids[ j ];
          if(residual_points.intensity < 0)
          {
              continue;
          }
          float point_len = sqrt( point.x * point.x + point.y * point.y + point.z * point.z);
          float residual_points_len = sqrt( residual_points.x * residual_points.x + residual_points.y * residual_points.y + residual_points.z * residual_points.z );
          if(fabs( point_len - residual_points_len ) < m_residuals_len_threshold / para )
          {
              flag = 1;
          }
        }
        if(0 == flag)      
        {   
            cloudAfter->push_back( point );
        } 
    }
    if( cloudAfter->points.size() > numThreshold )
    {
        ( *cloud ) += ( *cloudAfter );        
    }
    else
    {
         ( *cloud ) += ( *cloudTemp );  
    }
}
void structure_matrixSquare(pcl::PointCloud<PointType>::Ptr cloud, std::vector<std::vector<PointFeature>> &matrix, Feature_type feature_type)
{
    PointFeature pf;
    int mapsize = matrix.size();
    for(size_t i = 0; i < cloud->size(); i++)
    {
        PointType p = cloud->points[i];
        pf.point = p;
        pf.m_feature_type = feature_type;
        pf.removeflag = 0;
        int x = std::floor(fabs(p.x - minx)/RemoveSquare);
        int y = std::floor(fabs(p.y - miny)/RemoveSquare);
        int z = std::floor(fabs(p.z - minz)/RemoveSquare);
        int xyz = x + y * rowx + z * rowx * columny;
                 
        if(mapsize > xyz )
        {
            matrix[xyz].push_back(pf);  
        }
    }
}
void remove_residual_feature_points_square(std::vector<pcl::PointXYZI> residuals_ids)
{
    std::vector<std::vector<PointFeature>> matrixSquare;
    pcl::PointCloud<PointType>::Ptr surfPointsFlatTemp(new pcl::PointCloud<PointType>());
    pcl::PointCloud<PointType>::Ptr surfPointsLessFlatTemp(new pcl::PointCloud<PointType>());
    pcl::PointCloud<PointType>::Ptr cornerPointsLessSharpTemp(new pcl::PointCloud<PointType>());
    pcl::PointCloud<PointType>::Ptr cornerPointsSharpTemp(new pcl::PointCloud<PointType>());
    pcl::PointCloud<PointType>::Ptr allCloud(new pcl::PointCloud<PointType>());

   (*allCloud) += (*surfPointsFlatTemp) += *surfPointsFlat;
   (*allCloud) += (*surfPointsLessFlatTemp) += (*surfPointsLessFlat);
   (*allCloud) += (*cornerPointsLessSharpTemp) += (*cornerPointsLessSharp);
   (*allCloud) += (*cornerPointsSharpTemp) +=(*cornerPointsSharp);
   if(allCloud->size() < 500)return; 
   surfPointsFlat->clear();
   surfPointsLessFlat->clear();
   cornerPointsLessSharp->clear();
   cornerPointsSharp->clear();
 
    for(size_t i = 0; i < allCloud->size(); i++)
    {
       PointType p = allCloud->points[i];
       maxx = std::max(p.x, maxx);
       minx = std::min(p.x, minx);

       maxy = std::max(p.y, maxy);
       miny = std::min(p.y, miny);

       maxz = std::max(p.z, maxz);
       minz = std::min(p.z, minz);
    }
   rowx =std::floor( fabs(maxx- minx )/RemoveSquare );
   columny = std::floor( fabs(maxy - miny) / RemoveSquare);
   highz = std::floor ( fabs( maxz - minz )/ RemoveSquare);

   matrixSquare.resize( (rowx +1 ) * ( columny+1) * ( highz+1) );

   structure_matrixSquare(surfPointsFlatTemp, matrixSquare, Feature_type::e_surfPointsFlat);
   structure_matrixSquare(surfPointsLessFlatTemp, matrixSquare, Feature_type::e_surfPointsLessFlat);
   structure_matrixSquare(cornerPointsLessSharpTemp, matrixSquare, Feature_type::e_cornerPointsLessSharp);
   structure_matrixSquare(cornerPointsSharpTemp, matrixSquare, Feature_type::e_cornerPointsSharp);
   pcl::PointXYZI residual_points;
   float para = 0.0;
 
   for(size_t i = 0; i < residuals_ids.size() ; i++)
   {
        residual_points = residuals_ids[i];
        if(residual_points.intensity < 0)
        {
            continue;
        }
        int rx =std::floor( fabs(residual_points.x- minx )/RemoveSquare );
        int cy = std::floor( fabs(residual_points.y - miny) / RemoveSquare);
        int hz = std::floor ( fabs( residual_points.z - minz )/ RemoveSquare);
        int xyz = rx + cy * rowx + hz * rowx * columny;
        
        if(xyz >= rowx * columny * highz && xyz < 0 )
        {
            continue;
        }
        for(size_t j = 0; j < matrixSquare[xyz].size(); j++)
        {
            PointType point = matrixSquare[xyz][j].point;
            if(matrixSquare[xyz][j].m_feature_type == Feature_type::e_surfPointsFlat
            || matrixSquare[xyz][j].m_feature_type == Feature_type::e_cornerPointsSharp)
            {
                para = 1.0;
            }
            else
            {
                para = 5.0;
            }
            float point_len = sqrt( point.x * point.x + point.y * point.y + point.z * point.z);
            float residual_points_len = sqrt( residual_points.x * residual_points.x + residual_points.y * residual_points.y + residual_points.z * residual_points.z );
            if(fabs( point_len - residual_points_len ) < m_residuals_len_threshold / para )
            {
                matrixSquare[xyz][j].removeflag = 1;
            }            
        }
   }

   PointType po;
   for(size_t i = 0; i < matrixSquare.size(); i++)
   {
        for(size_t j = 0; j < matrixSquare[i].size(); j++)
        {
            po = matrixSquare[i][j].point;
            if(!matrixSquare[i][j].removeflag)
            {
                switch(matrixSquare[i][j].m_feature_type)
                {
                    case Feature_type::e_surfPointsFlat:
                        surfPointsFlat->push_back(po);
                        break;
                    case Feature_type::e_surfPointsLessFlat:
                        surfPointsLessFlat->push_back(po);
                        break;
                    case Feature_type::e_cornerPointsLessSharp:
                        cornerPointsLessSharp->push_back(po);
                        break;
                    case Feature_type::e_cornerPointsSharp:
                        cornerPointsSharp->push_back(po);
                        break;
                }
            }
        }
   }

    if(surfPointsFlat->size() < minFlatCloudSize)
    {
        surfPointsFlat->clear();
        *surfPointsFlat += *surfPointsFlatTemp;
    }
    if(surfPointsLessFlat->size() < minLessFlatCloudSize)
    {
         surfPointsLessFlat->clear();
        *surfPointsLessFlat += *surfPointsLessFlatTemp;
    }
    if(cornerPointsLessSharp->size() < minLessSharpCloudSize)
    {
        cornerPointsLessSharp->clear();
        *cornerPointsLessSharp += *cornerPointsLessSharpTemp;
    }
    if(cornerPointsSharp->size() < minSharpCloudSize)
    {
        cornerPointsSharp->clear();
        *cornerPointsSharp += *cornerPointsSharpTemp;
    }

}

void PreImuOdomDeskewInfo(double timeScanCur)
{
    preImuInitalFlag = true;
}

void getLaserAfterMapPose(const nav_msgs::Odometry::ConstPtr&  laserAferMapPose)
{

    mAfterMapPoseBuf.lock();
    afterMapPoseBuf.push( laserAferMapPose );
    mAfterMapPoseBuf.unlock();

}
bool checkAfterMapTime(double timeSur)
{

   static bool checkAfterFlag  = false;
   if(afterMapPoseBuf.size() < 1  &&  !checkAfterFlag)
   {
       checkAfterFlag = true;
       return true;
   }
   if(afterMapPoseBuf.size() < 1){
       usleep(200);
       return false;
   }

   double afterMapTime =  afterMapPoseBuf.front()->header.stamp.toSec();
   printf("***************afterTime :%lf   timeSur:%f \n",afterMapTime, timeSur);

   if(afterMapTime < 0.15)
   { 
       return true;
   }
   if(timeSur < afterMapTime - 0.15)
   {
       return false;
   }
   else
   {
        mAfterMapPoseBuf.lock();    
        nav_msgs::Odometry::ConstPtr afterMapPose = afterMapPoseBuf.front();
        afterMapPoseBuf.pop();
        mAfterMapPoseBuf.unlock();   

        double roll, pitch, yaw;
        geometry_msgs::Quaternion geoQuat = afterMapPose->pose.pose.orientation;
        tf::Matrix3x3(tf::Quaternion(geoQuat.z, geoQuat.x, geoQuat.y, geoQuat.w)).getRPY(roll, pitch, yaw);

        lastLaserPose[0] = afterMapPose->pose.pose.position.x;
        lastLaserPose[1] = afterMapPose->pose.pose.position.y;    
        lastLaserPose[2] = afterMapPose->pose.pose.position.z;
        lastLaserPose[3] = roll;
        lastLaserPose[4] = pitch;
        lastLaserPose[5] = yaw;

        t_w_curr[0] = lastLaserPose[0];
        t_w_curr[1] = lastLaserPose[1];    
        t_w_curr[2] = lastLaserPose[2];

        q_w_curr.x() = geoQuat.x;
        q_w_curr.y() = geoQuat.y; 
        q_w_curr.z() = geoQuat.z;
        q_w_curr.w() = geoQuat.w;
        return true;

   }


}

Eigen::Affine3f trans2Affine3f(float transformIn[])
{
    return pcl::getTransformation(transformIn[3], transformIn[4], transformIn[5], transformIn[0], transformIn[1], transformIn[2]);

}
void updateInitialGuess()
{
    // use imu pre-integration estimation for pose guess  
    static bool lastImuPreTransAvailable = false;
    static Eigen::Affine3f lastImuPreTransformation;
    if (!lastImuPreTransAvailable) {
        preTreatment[0] = initialGuessX;
        preTreatment[1] = initialGuessY;
        preTreatment[2] = initialGuessZ;
        preTreatment[3] = initialGuessRoll;
        preTreatment[4] = initialGuessPitch;
        preTreatment[5] = initialGuessYaw; 
        
    }
    /* 
    // use imu pre-integration estimation for pose guess  
    static bool lastImuPreTransAvailable = false;
    static Eigen::Affine3f lastImuPreTransformation;
    if (preImuInitalFlag == true)
    {
        Eigen::Affine3f transBack = pcl::getTransformation(initialGuessX,    initialGuessY,     initialGuessZ, 
                                                           initialGuessRoll, initialGuessPitch, initialGuessYaw);
        if (lastImuPreTransAvailable == false)
        {
            lastImuPreTransformation = transBack;
            lastImuPreTransAvailable = true;
        } else {
            Eigen::Affine3f transIncre = lastImuPreTransformation.inverse() * transBack;
            
            Eigen::Affine3f transTobe = trans2Affine3f( lastLaserPose );
            Eigen::Affine3f transFinal = transTobe * transIncre;
            pcl::getTranslationAndEulerAngles(transFinal, preTreatment[0], preTreatment[1], preTreatment[2], 
                                                          preTreatment[3], preTreatment[4], preTreatment[5]);

            lastImuPreTransformation = transBack;

            return;
        }
    }
    */
}
void updatePrePos(float prePos[]) {
    Eigen::Vector3d att_tmp = q2att(q_last_curr);
    for(size_t i = 0; i < 3; ++i) {
        prePos[i] = t_last_curr[i];
        prePos[i + 3] = att_tmp[i];
    }
}

void updatePreMatch(float prePos[])
{
    Eigen::Quaterniond  q = a2qua(prePos[3], prePos[4], prePos[5]);
    para_q[0] = q.x(); para_q[1] = q.y() ; para_q[2] = q.z() ; para_q[3] = q.w();

    para_t[0] = prePos[0] ; para_t[1] = prePos[1] ; para_t[2] = prePos[2];
    printf("***********************************\n");
    printf("para_t :%f  %f  %f \n",para_t[0], para_t[1], para_t[2]);
    printf("prePos :%f  %f  %f \n",prePos[0], prePos[1], prePos[2] );

    printf("para_q:%f  %f  %f  %f\n",para_q[0],para_q[1],para_q[2],para_q[3]);
    printf("pre_Q :%f  %f  %f  %f\n",q.x(), q.y(), q.z(), q.w());

}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "laserOdometry");
    ros::NodeHandle nh;

    nh.param<int>("mapping_skip_frame", skipFrameNum, 2);
    ROS_DEBUG("Mapping %d Hz \n", 10 / skipFrameNum);

    for(size_t i = 0; i < 6; i ++){
        lastLaserPose[i] = 0.0;
        preTreatment[i] = 0.0;
    }

    ros::Subscriber subCornerPointsSharp = nh.subscribe<sensor_msgs::PointCloud2>("/laser_cloud_sharp", 100, laserCloudSharpHandler);

    ros::Subscriber subCornerPointsLessSharp = nh.subscribe<sensor_msgs::PointCloud2>("/laser_cloud_less_sharp", 100, laserCloudLessSharpHandler);

    ros::Subscriber subSurfPointsFlat = nh.subscribe<sensor_msgs::PointCloud2>("/laser_cloud_flat", 100, laserCloudFlatHandler);

    ros::Subscriber subSurfPointsLessFlat = nh.subscribe<sensor_msgs::PointCloud2>("/laser_cloud_less_flat", 100, laserCloudLessFlatHandler);

    ros::Subscriber subLaserCloudFullRes = nh.subscribe<sensor_msgs::PointCloud2>("/velodyne_cloud_2", 100, laserCloudFullResHandler);

    //publish the  lidar pose before cloud matching   
    ros::Publisher pubLaserBeforeOdometry = nh.advertise<nav_msgs::Odometry>("/before_laser_odom", 10);

    ros::Subscriber subLaserAfterMapPose = nh.subscribe<nav_msgs::Odometry>("/aft_mapped_to_init", 100,getLaserAfterMapPose);

    ros::Publisher pubLaserCloudCornerLast = nh.advertise<sensor_msgs::PointCloud2>("/laser_cloud_corner_last", 100);

    ros::Publisher pubLaserCloudSurfLast = nh.advertise<sensor_msgs::PointCloud2>("/laser_cloud_surf_last", 100);

    ros::Publisher pubLaserCloudFullRes = nh.advertise<sensor_msgs::PointCloud2>("/velodyne_cloud_3", 100);

    ros::Publisher pubLaserOdometry = nh.advertise<nav_msgs::Odometry>("/laser_odom_to_init", 100);



    ros::Publisher pubLaserPath = nh.advertise<nav_msgs::Path>("/laser_odom_path", 100);

    nav_msgs::Path laserPath;

    int frameCount = 0;
    ros::Rate rate(100);

    while (ros::ok())
    {
        ros::spinOnce();
        if (systemInited) {
            ROS_DEBUG("LaserOdo cornerSharpBuf size:%d\n",cornerSharpBuf.size());
            ROS_DEBUG("LaserOdo cornerLessSharpBuf size:%d\n",cornerLessSharpBuf.size());
            ROS_DEBUG("LaserOdo surfFlatBuf size:%d\n",surfFlatBuf.size());
            ROS_DEBUG("LaserOdo surfLessFlatBuf size:%d\n",surfLessFlatBuf.size());    
            ROS_DEBUG("LaserOdo fullPointsBuf size:%d\n",fullPointsBuf.size());                     
        }
        if (!cornerSharpBuf.empty() && !cornerLessSharpBuf.empty() &&
            !surfFlatBuf.empty() && !surfLessFlatBuf.empty() &&
            !fullPointsBuf.empty())
        {
            ROS_DEBUG("LaserOdo  ----------------------10___\n");
            timeCornerPointsSharp = cornerSharpBuf.front()->header.stamp.toSec();
            timeCornerPointsLessSharp = cornerLessSharpBuf.front()->header.stamp.toSec();
            timeSurfPointsFlat = surfFlatBuf.front()->header.stamp.toSec();
            timeSurfPointsLessFlat = surfLessFlatBuf.front()->header.stamp.toSec();
            timeLaserCloudFullRes = fullPointsBuf.front()->header.stamp.toSec();

            if (timeCornerPointsSharp != timeLaserCloudFullRes ||
                timeCornerPointsLessSharp != timeLaserCloudFullRes ||
                timeSurfPointsFlat != timeLaserCloudFullRes ||
                timeSurfPointsLessFlat != timeLaserCloudFullRes )
            {
                ROS_DEBUG("unsync messeage!");
                ROS_BREAK();
            }
            ROS_DEBUG("------------------------11___\n");            
            if(!checkAfterMapTime(timeLaserCloudFullRes))
            {
               ROS_DEBUG("LaserOdo checkAfterMapTime ----------\n");
               continue;
            }
            ROS_DEBUG("LaserOdo ------------------------1___\n");              
            mBuf.lock();
            cornerPointsSharp->clear();
            pcl::fromROSMsg(*cornerSharpBuf.front(), *cornerPointsSharp);
            cornerSharpBuf.pop();

            cornerPointsLessSharp->clear();
            pcl::fromROSMsg(*cornerLessSharpBuf.front(), *cornerPointsLessSharp);
            cornerLessSharpBuf.pop();

            surfPointsFlat->clear();
            pcl::fromROSMsg(*surfFlatBuf.front(), *surfPointsFlat);
            surfFlatBuf.pop();

            surfPointsLessFlat->clear();
            pcl::fromROSMsg(*surfLessFlatBuf.front(), *surfPointsLessFlat);
            surfLessFlatBuf.pop();

            laserCloudFullRes->clear();
            pcl::fromROSMsg(*fullPointsBuf.front(), *laserCloudFullRes);
            fullPointsBuf.pop();
            mBuf.unlock();

            PreImuOdomDeskewInfo( timeSurfPointsLessFlat );
            ROS_DEBUG("LaserOdo ----------------------1\n");
            //publish lidar's before matching's pose
            nav_msgs::Odometry  before_laser_match_pose;
            before_laser_match_pose.header.stamp = ros::Time().fromSec( timeSurfPointsLessFlat );
			before_laser_match_pose.pose.pose.orientation.x = q_w_curr.x();
			before_laser_match_pose.pose.pose.orientation.y = q_w_curr.y();
			before_laser_match_pose.pose.pose.orientation.z = q_w_curr.z();
			before_laser_match_pose.pose.pose.orientation.w = q_w_curr.w();
			before_laser_match_pose.pose.pose.position.x = t_w_curr.x();
			before_laser_match_pose.pose.pose.position.y = t_w_curr.y();
			before_laser_match_pose.pose.pose.position.z = t_w_curr.z();
            ROS_INFO("LaserOdo before_laser_odom x:%f  %f  %f \n",t_w_curr.x(),t_w_curr.y(),t_w_curr.z());
            pubLaserBeforeOdometry.publish( before_laser_match_pose );

            TicToc t_whole;
            // initializing
            if (!systemInited)
            {
                systemInited = true;
                std::cout << "Initialization finished \n";
            }
            else
            {           

                int cornerPointsSharpNum = cornerPointsSharp->points.size();
                int surfPointsFlatNum = surfPointsFlat->points.size();


                updateInitialGuess();
                updatePrePos( preTreatment );
                updatePreMatch( preTreatment );

                TicToc t_opt;
                std::vector<pcl::PointXYZI> max_residual_point_ids;
                for (size_t opti_counter = 0; opti_counter < 2; ++opti_counter)
                {
                    corner_correspondence = 0;
                    plane_correspondence = 0;

                    //ceres::LossFunction *loss_function = NULL;
                    ceres::LossFunction *loss_function = new ceres::HuberLoss(0.1);
                    ceres::LocalParameterization *q_parameterization =
                        new ceres::EigenQuaternionParameterization();
                    ceres::Problem::Options problem_options;

                    ceres::Problem problem(problem_options);
                    problem.AddParameterBlock(para_q, 4, q_parameterization);
                    problem.AddParameterBlock(para_t, 3);

                    ROS_INFO("LaserOdo para_t :    %f  %f  %f\n",para_t[0],para_t[1],para_t[2]);
                    ROS_INFO("LaserOdo t_last_curr %f  %f  %f\n",t_last_curr[0],t_last_curr[1],t_last_curr[2]);

                    pcl::PointXYZI pointSel;
                    std::vector<int> pointSearchInd;
                    std::vector<float> pointSearchSqDis;
                    //remove outliers,dynamic objects filtering livox
                    ceres::ResidualBlockId              block_id;
                    std::vector<ceres::ResidualBlockId> residual_block_ids;
                    max_residual_point_ids.clear();
                    TicToc t_data;
                    // find correspondence for corner features
                    for (int i = 0; i < cornerPointsSharpNum; ++i)
                    {
                        TransformToStart(&(cornerPointsSharp->points[i]), &pointSel);
                        kdtreeCornerLast->nearestKSearch(pointSel, 1, pointSearchInd, pointSearchSqDis);

                        int closestPointInd = -1, minPointInd2 = -1;
                        if (pointSearchSqDis[0] < DISTANCE_SQ_THRESHOLD)
                        {
                            closestPointInd = pointSearchInd[0];
                            int closestPointScanID = int(laserCloudCornerLast->points[closestPointInd].intensity);

                            double minPointSqDis2 = DISTANCE_SQ_THRESHOLD;
                                        // search in the direction of increasing scan line
                            for (int j = closestPointInd + 1; j < (int)laserCloudCornerLast->points.size(); ++j)
                            {
                                // if in the same scan line, continue
                                if (int(laserCloudCornerLast->points[j].intensity) <= closestPointScanID)
                                    continue;

                                // if not in nearby scans, end the loop
                                if (int(laserCloudCornerLast->points[j].intensity) > (closestPointScanID + NEARBY_SCAN))
                                    break;

                                double pointSqDis = (laserCloudCornerLast->points[j].x - pointSel.x) *
                                                        (laserCloudCornerLast->points[j].x - pointSel.x) +
                                                    (laserCloudCornerLast->points[j].y - pointSel.y) *
                                                        (laserCloudCornerLast->points[j].y - pointSel.y) +
                                                    (laserCloudCornerLast->points[j].z - pointSel.z) *
                                                        (laserCloudCornerLast->points[j].z - pointSel.z);

                                if (pointSqDis < minPointSqDis2)
                                {
                                    // find nearer point
                                    minPointSqDis2 = pointSqDis;
                                    minPointInd2 = j;
                                }
                            }
                            // search in the direction of decreasing scan line
                            for (int j = closestPointInd - 1; j >= 0; --j)
                            {
                                // if in the same scan line, continue
                                if (int(laserCloudCornerLast->points[j].intensity) >= closestPointScanID)
                                    continue;

                                // if not in nearby scans, end the loop
                                if (int(laserCloudCornerLast->points[j].intensity) < (closestPointScanID - NEARBY_SCAN))
                                    break;

                                double pointSqDis = (laserCloudCornerLast->points[j].x - pointSel.x) *
                                                        (laserCloudCornerLast->points[j].x - pointSel.x) +
                                                    (laserCloudCornerLast->points[j].y - pointSel.y) *
                                                        (laserCloudCornerLast->points[j].y - pointSel.y) +
                                                    (laserCloudCornerLast->points[j].z - pointSel.z) *
                                                        (laserCloudCornerLast->points[j].z - pointSel.z);

                                if (pointSqDis < minPointSqDis2)
                                {
                                    // find nearer point
                                    minPointSqDis2 = pointSqDis;
                                    minPointInd2 = j;
                                }
                            }
                        }
                        if (minPointInd2 >= 0) // both closestPointInd and minPointInd2 is valid
                        {
                            Eigen::Vector3d curr_point(cornerPointsSharp->points[i].x,
                                                       cornerPointsSharp->points[i].y,
                                                       cornerPointsSharp->points[i].z);
                            Eigen::Vector3d last_point_a(laserCloudCornerLast->points[closestPointInd].x,
                                                         laserCloudCornerLast->points[closestPointInd].y,
                                                         laserCloudCornerLast->points[closestPointInd].z);
                            Eigen::Vector3d last_point_b(laserCloudCornerLast->points[minPointInd2].x,
                                                         laserCloudCornerLast->points[minPointInd2].y,
                                                         laserCloudCornerLast->points[minPointInd2].z);

                            double s;
                            if (DISTORTION)
                                s = (cornerPointsSharp->points[i].intensity - int(cornerPointsSharp->points[i].intensity)) / SCAN_PERIOD;
                            else
                                s = 1.0;
                            ceres::CostFunction *cost_function = LidarEdgeFactor::Create(curr_point, last_point_a, last_point_b, s);
                            block_id= problem.AddResidualBlock(cost_function, loss_function, para_q, para_t);
                            residual_block_ids.push_back(block_id);
                            pcl::PointXYZI ptemp=cornerPointsSharp->points[i];
                            ptemp.intensity= i;
                            max_residual_point_ids.push_back(ptemp);//notes: Uncertain "pointSel"
                            corner_correspondence++;
                        }
                    }

                    // find correspondence for plane features
                    for (int i = 0; i < surfPointsFlatNum; ++i)
                    {
                        TransformToStart(&(surfPointsFlat->points[i]), &pointSel);
                        kdtreeSurfLast->nearestKSearch(pointSel, 1, pointSearchInd, pointSearchSqDis);

                        int closestPointInd = -1, minPointInd2 = -1, minPointInd3 = -1;
                        if (pointSearchSqDis[0] < DISTANCE_SQ_THRESHOLD)
                        {
                            closestPointInd = pointSearchInd[0];

                            // get closest point's scan ID
                            int closestPointScanID = int(laserCloudSurfLast->points[closestPointInd].intensity);
                            double minPointSqDis2 = DISTANCE_SQ_THRESHOLD, minPointSqDis3 = DISTANCE_SQ_THRESHOLD;

                            // search in the direction of increasing scan line
                            for (int j = closestPointInd + 1; j < (int)laserCloudSurfLast->points.size(); ++j)
                            {
                                // if not in nearby scans, end the loop
                                if (int(laserCloudSurfLast->points[j].intensity) > (closestPointScanID + NEARBY_SCAN))
                                    break;

                                double pointSqDis = (laserCloudSurfLast->points[j].x - pointSel.x) *
                                                        (laserCloudSurfLast->points[j].x - pointSel.x) +
                                                    (laserCloudSurfLast->points[j].y - pointSel.y) *
                                                        (laserCloudSurfLast->points[j].y - pointSel.y) +
                                                    (laserCloudSurfLast->points[j].z - pointSel.z) *
                                                        (laserCloudSurfLast->points[j].z - pointSel.z);

                                // if in the same or lower scan line
                                if (int(laserCloudSurfLast->points[j].intensity) <= closestPointScanID && pointSqDis < minPointSqDis2)
                                {
                                    minPointSqDis2 = pointSqDis;
                                    minPointInd2 = j;
                                }
                                // if in the higher scan line
                                else if (int(laserCloudSurfLast->points[j].intensity) > closestPointScanID && pointSqDis < minPointSqDis3)
                                {
                                    minPointSqDis3 = pointSqDis;
                                    minPointInd3 = j;
                                }
                            }

                            // search in the direction of decreasing scan line
                            for (int j = closestPointInd - 1; j >= 0; --j)
                            {
                                // if not in nearby scans, end the loop
                                if (int(laserCloudSurfLast->points[j].intensity) < (closestPointScanID - NEARBY_SCAN))
                                    break;

                                double pointSqDis = (laserCloudSurfLast->points[j].x - pointSel.x) *
                                                        (laserCloudSurfLast->points[j].x - pointSel.x) +
                                                    (laserCloudSurfLast->points[j].y - pointSel.y) *
                                                        (laserCloudSurfLast->points[j].y - pointSel.y) +
                                                    (laserCloudSurfLast->points[j].z - pointSel.z) *
                                                        (laserCloudSurfLast->points[j].z - pointSel.z);

                                // if in the same or higher scan line
                                if (int(laserCloudSurfLast->points[j].intensity) >= closestPointScanID && pointSqDis < minPointSqDis2)
                                {
                                    minPointSqDis2 = pointSqDis;
                                    minPointInd2 = j;
                                }
                                else if (int(laserCloudSurfLast->points[j].intensity) < closestPointScanID && pointSqDis < minPointSqDis3)
                                {
                                    // find nearer point
                                    minPointSqDis3 = pointSqDis;
                                    minPointInd3 = j;
                                }
                            }

                            if (minPointInd2 >= 0 && minPointInd3 >= 0)
                            {

                                Eigen::Vector3d curr_point(surfPointsFlat->points[i].x,
                                                            surfPointsFlat->points[i].y,
                                                            surfPointsFlat->points[i].z);
                                Eigen::Vector3d last_point_a(laserCloudSurfLast->points[closestPointInd].x,
                                                                laserCloudSurfLast->points[closestPointInd].y,
                                                                laserCloudSurfLast->points[closestPointInd].z);
                                Eigen::Vector3d last_point_b(laserCloudSurfLast->points[minPointInd2].x,
                                                                laserCloudSurfLast->points[minPointInd2].y,
                                                                laserCloudSurfLast->points[minPointInd2].z);
                                Eigen::Vector3d last_point_c(laserCloudSurfLast->points[minPointInd3].x,
                                                                laserCloudSurfLast->points[minPointInd3].y,
                                                                laserCloudSurfLast->points[minPointInd3].z);

                                double s;
                                if (DISTORTION)
                                    s = (surfPointsFlat->points[i].intensity - int(surfPointsFlat->points[i].intensity)) / SCAN_PERIOD;
                                else
                                    s = 1.0;
                                ceres::CostFunction *cost_function = LidarPlaneFactor::Create(curr_point, last_point_a, last_point_b, last_point_c, s);
                                block_id = problem.AddResidualBlock(cost_function, loss_function, para_q, para_t);
                                residual_block_ids.push_back(block_id);
                                pcl::PointXYZI ptemp=surfPointsFlat->points[i];
                                ptemp.intensity= i+ cornerPointsSharpNum;
                                max_residual_point_ids.push_back(ptemp);//notes: Uncertain "pointSel"
                                plane_correspondence++;
                            }
                        }
                    }
                    //printf("coner_correspondance %d, plane_correspondence %d \n", corner_correspondence, plane_correspondence);
                //    printf("data association time %f ms \n", t_data.toc());

                    if ((corner_correspondence + plane_correspondence) < 10)
                    {
                        ROS_WARN("LaserOdo less correspondence! *************************************************\n");
                    }
                    TicToc t_solver;
                    ceres::Solver::Options options;
                    options.linear_solver_type = ceres::DENSE_QR;
                    options.max_num_iterations = 4;
                    options.minimizer_progress_to_stdout = false;
                    ceres::Solver::Summary summary;
                    ceres::Solve(options, &problem, &summary);
                   
                    std::vector< ceres::ResidualBlockId> residual_block_ids_temp;
                    residual_block_ids_temp.resize(residual_block_ids.size());
                    residual_block_ids_temp.clear();
                    ceres::Problem::EvaluateOptions eval_options;
                    eval_options.residual_blocks = residual_block_ids;
                    double  total_cost=0.0;
                    std::vector<double> residuals;
                    problem.Evaluate(eval_options,&total_cost,&residuals,nullptr,nullptr);
                    
                    double m_inliner_ration_threshold = compute_inlier_residual_threshold(residuals,m_inlier_ratio);
                    m_inlier_threshold =  std::max(m_inliner_dis,m_inliner_ration_threshold);
                    int remove_residuals_num =0;
                    for(unsigned int i=0; i< residual_block_ids.size(); i++)
                    {
                        if( (fabs( residuals[ 3*i + 0 ])+ fabs( residuals[ 3*i + 1 ])+fabs( residuals[ 3*i + 2 ])) > m_inlier_threshold)
                        {                               
                            problem.RemoveResidualBlock(residual_block_ids[i]);
                            remove_residuals_num++;
                        }
                        else
                        {
                            max_residual_point_ids[i].intensity=max_residual_point_ids[i].intensity*-1;
                            residual_block_ids_temp.push_back( residual_block_ids[i]);
                        }
                    }   
                    residual_block_ids = residual_block_ids_temp;
                    ceres::Solve( options, &problem, &summary );
                 //   printf("remove_residuals_num :%d\n",remove_residuals_num);
                    
                 //   printf("solver time %f ms \n", t_solver.toc());
                }
            //    printf("optimization twice time %f \n", t_opt.toc());


                t_w_curr = t_w_curr + q_w_curr * t_last_curr;
                q_w_curr = q_w_curr * q_last_curr;
            //    printf("t_w_curr: %f  %f  %f\n",t_w_curr[0],t_w_curr[1],t_w_curr[2]);
            //    printf("-----------------------------------------\n");
                
            //   printf("-surfPointsFlat size:%d  cornerPointsSharp  size:%d\n",( int )surfPointsFlat->size(),( int )cornerPointsSharp->size());    
             //   printf("-surflessFlat size:%d   cornerlessSharp size:%d\n",( int )surfPointsLessFlat->size(),( int )cornerPointsLessSharp->size());   
     
                TicToc remove_residual_point_time;                                               
            //    remove_residual_feature_points( surfPointsFlat , max_residual_point_ids , 40 , 1.0 );
            //    remove_residual_feature_points( surfPointsLessFlat , max_residual_point_ids , 2000 , 5.0);  
            //    remove_residual_feature_points( cornerPointsLessSharp , max_residual_point_ids , 500 , 5.0 );            
             //   remove_residual_feature_points( cornerPointsSharp , max_residual_point_ids , 80 ,1.0 );
                 remove_residual_feature_points_square(max_residual_point_ids);
               //  printf("&&&&&&remove_residual_point_time:%f ms\n",remove_residual_point_time.toc());
               //  printf("@surflessFlat size:%d   cornerlessSharp size:%d\n",( int )surfPointsLessFlat->size(),( int )cornerPointsLessSharp->size());  
              //   printf("@surfPointsFlat size:%d  cornerPointsSharp  size:%d\n",( int )surfPointsFlat->size(),( int )cornerPointsSharp->size());                  
                
            }
           
            TicToc t_pub;
            // publish odometry
            nav_msgs::Odometry laserOdometry;
            laserOdometry.header.frame_id = "camera_init";
            laserOdometry.child_frame_id = "laser_odom";
            laserOdometry.header.stamp = ros::Time().fromSec(timeSurfPointsLessFlat);
            laserOdometry.pose.pose.orientation.x = q_w_curr.x();
            laserOdometry.pose.pose.orientation.y = q_w_curr.y();
            laserOdometry.pose.pose.orientation.z = q_w_curr.z();
            laserOdometry.pose.pose.orientation.w = q_w_curr.w();
            laserOdometry.pose.pose.position.x = t_w_curr.x();
            laserOdometry.pose.pose.position.y = t_w_curr.y();
            laserOdometry.pose.pose.position.z = t_w_curr.z();
            ROS_DEBUG("LaserOdo *************pubLaserOdometry\n");
            pubLaserOdometry.publish(laserOdometry);

            geometry_msgs::PoseStamped laserPose; 
            laserPose.header = laserOdometry.header;
            laserPose.pose = laserOdometry.pose.pose;
            laserPath.header.stamp = laserOdometry.header.stamp;
            laserPath.poses.push_back(laserPose);
            laserPath.header.frame_id = "camera_init";
            pubLaserPath.publish(laserPath);
            ROS_DEBUG("LaserOdo------------------------2\n");
            // transform corner features and plane features to the scan end point
            if (0)
            {
                int cornerPointsLessSharpNum = cornerPointsLessSharp->points.size();
                for (int i = 0; i < cornerPointsLessSharpNum; i++)
                {
                    TransformToEnd(&cornerPointsLessSharp->points[i], &cornerPointsLessSharp->points[i]);
                }

                int surfPointsLessFlatNum = surfPointsLessFlat->points.size();
                for (int i = 0; i < surfPointsLessFlatNum; i++)
                {
                    TransformToEnd(&surfPointsLessFlat->points[i], &surfPointsLessFlat->points[i]);
                }

                int laserCloudFullResNum = laserCloudFullRes->points.size();
                for (int i = 0; i < laserCloudFullResNum; i++)
                {
                    TransformToEnd(&laserCloudFullRes->points[i], &laserCloudFullRes->points[i]);
                }
            }

            pcl::PointCloud<PointType>::Ptr laserCloudTemp = cornerPointsLessSharp;
            cornerPointsLessSharp = laserCloudCornerLast;
            laserCloudCornerLast = laserCloudTemp;

            laserCloudTemp = surfPointsLessFlat;
            surfPointsLessFlat = laserCloudSurfLast;
            laserCloudSurfLast = laserCloudTemp;

            laserCloudCornerLastNum = laserCloudCornerLast->points.size();
            laserCloudSurfLastNum = laserCloudSurfLast->points.size();

            // std::cout << "the size of corner last is " << laserCloudCornerLastNum << ", and the size of surf last is " << laserCloudSurfLastNum << '\n';
            ROS_DEBUG("LaserOdo------------------------3\n");
            kdtreeCornerLast->setInputCloud(laserCloudCornerLast);
            kdtreeSurfLast->setInputCloud(laserCloudSurfLast);

            if (frameCount % skipFrameNum == 0)
            {
                frameCount = 0;

                sensor_msgs::PointCloud2 laserCloudCornerLast2;
                pcl::toROSMsg(*laserCloudCornerLast, laserCloudCornerLast2);
                laserCloudCornerLast2.header.stamp = ros::Time().fromSec(timeSurfPointsLessFlat);
                laserCloudCornerLast2.header.frame_id = "camera_init";
                pubLaserCloudCornerLast.publish(laserCloudCornerLast2);

                sensor_msgs::PointCloud2 laserCloudSurfLast2;
                pcl::toROSMsg(*laserCloudSurfLast, laserCloudSurfLast2);
                laserCloudSurfLast2.header.stamp = ros::Time().fromSec(timeSurfPointsLessFlat);
                laserCloudSurfLast2.header.frame_id = "camera_init";
                pubLaserCloudSurfLast.publish(laserCloudSurfLast2);

                sensor_msgs::PointCloud2 laserCloudFullRes3;
                pcl::toROSMsg(*laserCloudFullRes, laserCloudFullRes3);
                laserCloudFullRes3.header.stamp = ros::Time().fromSec(timeSurfPointsLessFlat);
                laserCloudFullRes3.header.frame_id = "camera_init";
                pubLaserCloudFullRes.publish(laserCloudFullRes3);
            }
         //   printf("publication time %f ms \n", t_pub.toc());
         //   printf("whole laserOdometry time %f ms \n \n", t_whole.toc());
            if(t_whole.toc() > 100)
           //     ROS_WARN("odometry process over 100ms");
            ROS_DEBUG("LaserOdo------------------------4\n");
            frameCount++;
        }
        rate.sleep();
    }
    return 0;
}